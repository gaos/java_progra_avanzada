package biblioteca;

public class libros implements controlador {
	private String codigo;
	private String titulo;
	private int año;
	private boolean prestado;
	private int tiempo;
	public libros(String codigo, String titulo,int año, int tiempo) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.año = año;
		this.tiempo = tiempo;
		this.prestado = false;
	}
	
	
	public int getTiempo() {
		return tiempo;
	}


	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}


	public boolean isPrestado() {
		return prestado;
	}

	@Override
	public void prestar() {
		this.prestado = true;
	}

	@Override
	public void devolver() {
		this.prestado = false;
		
	}
	
	public void muestra_detalle() {
		System.out.println("Se titula:"+this.titulo+",codigo:"+this.codigo+
				" fue publicado el "+ this.año);
		if(prestado) {
			System.out.println("\nHa sido prestado");
		}else {
			System.out.println("\nNo ha sido prestado");
		}
	}
}
