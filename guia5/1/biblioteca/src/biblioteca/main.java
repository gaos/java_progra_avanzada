package biblioteca;
import java.util.*;
import java.time.*;

public class main {
	public static void cambiar(biblio biblioteca, int dia_inicio) {
		Scanner txt = new Scanner(System.in);
		String dat, cam;
		
		System.out.println("¿De donde desea cambiar? 1. libros. 2 revistas");
		dat = txt.nextLine();
		if (dat.equalsIgnoreCase("1")) {
			System.out.println("De los libros disponibles cual desea cambiar, presione el numero:");
			cam = txt.nextLine();
			try {
				int numero = Integer.parseInt(cam);
				if(!biblioteca.libro.get(numero).isPrestado()) {
					biblioteca.libro.get(numero).prestar();
					biblioteca.libro.get(numero).setTiempo(dia_inicio);
				}else {
					biblioteca.libro.get(numero).devolver();
					int a = dia_inicio - biblioteca.libro.get(numero).getTiempo();
					if( a > 5 ) {
						System.out.println("Tiene que pagar: "+ ((a - 5) * 1290) + " $");
					}
				}
			} catch (Exception e) {
				System.out.println("opcion invalida");
			}
		}else if (dat.equalsIgnoreCase("2")) {
			System.out.println("De los libros disponibles cual desea cambiar, presione el numero:");
			cam = txt.nextLine();
			try {
				int numero = Integer.parseInt(cam);
				if(!biblioteca.revista.get(numero).isPrestado()) {
					biblioteca.revista.get(numero).prestar();
				}else {
					biblioteca.revista.get(numero).devolver();
				}
			} catch (Exception e) {
				System.out.println("Opcion invalida");
			}
		}else {
			System.out.println("Opción invalida...");
		}
	}
	public static void mostrar_disponibles(biblio biblioteca) {
		System.out.println("Los libros disponibles son:"+ biblioteca.libro.size());
		for (int i = 0; i < biblioteca.libro.size(); i++) {
			System.out.println("Libro:"+i + ".");
			biblioteca.libro.get(i).muestra_detalle();
		}
		System.out.println("Las revistas disponibles son:"+ biblioteca.revista.size());
		for (int i = 0; i < biblioteca.revista.size(); i++) {
			System.out.println("Revista:"+i + ".");
			biblioteca.revista.get(i).muestra_detalle();
		}
	}

	public static void main(String[] args) {
		biblio biblioteca = new biblio();
		while(true) {
			String opc;
			Scanner txt = new Scanner(System.in);
			String dat;
			System.out.println("Desea añadir (1) libro o (2) revista: ");
			opc = txt.nextLine();
			if(opc.equalsIgnoreCase("1")) {
				String codigo, titulo, varia, varia2;
				int año, tiempo;
				System.out.println("Ingrese el titulo del libro:");
				titulo = txt.nextLine();
				System.out.println("Ingrese el codigo:");
				codigo = txt.nextLine();
				System.out.println("Ingrese el año del libro");
				varia = txt.nextLine();
				año = Integer.parseInt(varia);
				//por ahora 0
				tiempo = 0;
				libros librito = new libros(codigo, titulo, año, tiempo);
				biblioteca.añadir_libro(librito);
			}else if (opc.equalsIgnoreCase("2")){
				String codigo, titulo, varia, varia2, opc2, genero, varia3,varia4;
				int año, tiempo;
				System.out.println("Ingrese el titulo de la revista:");
				titulo = txt.nextLine();
				System.out.println("Ingrese el codigo:");
				codigo = txt.nextLine();
				System.out.println("Ingrese el año de la revista:");
				varia = txt.nextLine();
				año = Integer.parseInt(varia);
				tiempo = 0;
				System.out.println("¿Genero de la revista?\n1.deportivo");
				System.out.println("2.comics\n3.cientifica\n4.gastronomia");
				opc2 = txt.nextLine();
				switch (opc2) {
				case "1":
					genero = "deportivo";
					break;
				case "2":
					genero = "comics";
					break;
				case "3":
					genero = "cientifica";
					break;
				case "4":
					genero = "gastronomia";
					break;

				default:
					genero = "";
					System.out.println("Ingreso invalido");
					break;
				}
				int num_edi, caducidad;
				System.out.println("Ingrese la caducidad: ");
				varia3 = txt.nextLine();
				caducidad = Integer.parseInt(varia3);
				System.out.println("Ingrese el numero de edicion: ");
				varia4 = txt.nextLine();
				num_edi = Integer.parseInt(varia4);
				revistas revista = new revistas(codigo, titulo, año, tiempo, genero, num_edi, caducidad);
				biblioteca.añadir_revista(revista);
			}else {
				System.out.println("no ha elegido entre esos dos");
			}
			
			
			System.out.println("Presione q si quiere dejar de añadir:");
			dat = txt.nextLine();
			if(dat.equalsIgnoreCase("q")) {
				break;
			}
		}
		mostrar_disponibles(biblioteca);
		int dia_inicio = LocalDate.now().getDayOfYear();
		cambiar(biblioteca, dia_inicio);
		mostrar_disponibles(biblioteca);

		
		while(true) {
			System.out.println("Amanecer de un nuevo día.");
			dia_inicio = dia_inicio + 1;
			cambiar(biblioteca, dia_inicio);
			mostrar_disponibles(biblioteca);
			Scanner txt = new Scanner(System.in);
			System.out.println("Presione q si quiere dejar de pasar dias:");
			String dat2;
			dat2 = txt.nextLine();
			if(dat2.equalsIgnoreCase("q")) {
				break;
			}
		}
		

	}

}
