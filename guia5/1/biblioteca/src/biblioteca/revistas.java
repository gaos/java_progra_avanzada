package biblioteca;

public class revistas extends libros implements controlador {
	private String genero;
	private int caducidad;
	private int num_edicion;
	private boolean caducada;
	
	public revistas(String codigo, String titulo,int año, int tiempo, String genero, int num_edi, int caducidad) {
		super(codigo, titulo, año, tiempo);
		this.genero = genero;
		this.num_edicion = num_edi;
		this.caducidad = caducidad;
		if (tiempo > caducidad) {
			this.caducada = true;
		}else {
			this.caducada = false;
		}
	}
	public void setCaducada(boolean caducada) {
		this.caducada = caducada;
	}
	@Override
	public void prestar() {
		super.prestar();
	}
	@Override
	public void devolver() {
		super.devolver();
	}
	@Override
	public void muestra_detalle() {
		super.muestra_detalle();
		System.out.println("Pertenece al género:"+ this.genero+" numero de edición"+
		this.num_edicion+" caduca: "+ this.caducidad);
	}
	
	
	
}
