package buscador;

public class Administrativo extends Persona{
	String seccion;
	//constructor con herencia de los administrativos
	public Administrativo(String Anombre,String Aapellido,String Aidentificacion, int Aanexo,
							int Aincorporado, String seccion) {
		super(Anombre, Aapellido, Aidentificacion, Aanexo, Aincorporado, false);
		this.seccion = seccion;
	}
	public String getSeccion() {
		return seccion;
	}
}