package buscador;

public class Estudiante extends Persona{
	String curso;
	//constructor con herencia del estudiante
	public Estudiante(String eNombre, String eApellido, String eIdentificacion, String curso) {
		super(eNombre, eApellido, eIdentificacion);
		this.curso = curso;
	}
	public String getCurso() {
		return curso;
	}
}