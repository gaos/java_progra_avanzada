package buscador;
import java.util.*;
public class main {

	//se encarga de la función de buscar según el criterio identificación
	static void buscar(ArrayList<Estudiante> lista_estudiantes,
			ArrayList<Profesor> lista_profesores, 
			ArrayList<Administrativo> lista_administrativos) {
		
		ArrayList<Estudiante> posible1 = new ArrayList<Estudiante>();
		ArrayList<Profesor> posible2 = new ArrayList<Profesor>();
		ArrayList<Administrativo> posible3 = new ArrayList<Administrativo>();
		
		Scanner txt = new Scanner(System.in);
		String buscado;
		
		System.out.println("Identificación a buscar: ");
		buscado = txt.nextLine();
		//debido a que cada lista difiere su tipo de arraylist se realizo un for para cada una
		for (int i = 0; i < lista_estudiantes.size(); i++) {
			if(buscado.equalsIgnoreCase(lista_estudiantes.get(i).getIdentificacion())) {
				posible1.add(lista_estudiantes.get(i));
			}
		}
		for (int j = 0; j < lista_profesores.size(); j++) {
			if(buscado.equalsIgnoreCase(lista_profesores.get(j).getIdentificacion())) {
				posible2.add(lista_profesores.get(j));
			}
		}
		for (int j2 = 0; j2 < lista_administrativos.size(); j2++) {
			if(buscado.equalsIgnoreCase(lista_administrativos.get(j2).getIdentificacion())) {
				posible3.add(lista_administrativos.get(j2));
			}
		}
		//una vez añada la cantidad de las listas con esa identificación, se muestran la cantidad
		System.out.println("La cantidad de personas que hay con esa identificacion es: "+ (posible1.size()
																							+ posible2.size()
																							+ posible3.size()));
		System.out.println("En administracion hay:" + posible3.size());
		for (int i = 0; i < posible3.size(); i++) {
			System.out.println(posible3.get(i).getNombre() + " " +posible3.get(i).getApellido()+
								posible3.get(i).getSeccion()+ posible3.get(i).getAnexo()+
								posible3.get(i).getIncorporado());
		}
		System.out.println("En los docentes hay:" + posible2.size());
		for (int i = 0; i < posible2.size(); i++) {
			System.out.println(posible2.get(i).getNombre() + " " +posible2.get(i).getApellido()+
								posible2.get(i).getDepartamento()+ posible2.get(i).getAnexo()+
								posible2.get(i).getIncorporado());
		}
		System.out.println("En los estudiantes hay:" + posible1.size());
		for (int i = 0; i < posible1.size(); i++) {
			System.out.println(posible1.get(i).getNombre() + " " +posible1.get(i).getApellido());
		}
		
		
	}
	//para añadir los datos de las personas
	static String[] datos_p() {
		String[] datos = {"","",""};
		Scanner txt = new Scanner(System.in);
		String nombre, apellido, identificacion;
		System.out.println("Ingrese el nombre: ");
		nombre = txt.nextLine();
		System.out.println("Ingrese el apellido: ");
		apellido = txt.nextLine();
		System.out.println("Ingrese la identificación: ");
		identificacion = txt.nextLine();
		datos[0] = nombre;
		datos[1] = apellido;
		datos[2] = identificacion;
		return datos;
	}
	// datos generales de empleados
	static int[] datos_e() {
		int[] datos = {0,0};
		Scanner txt = new Scanner(System.in);
		String anexo, ano;
		System.out.println("Ingrese el numero de anexo: ");
		anexo = txt.nextLine();
		System.out.println("Ingrese el año de afiliación: ");
		ano = txt.nextLine();
		datos[0] = Integer.parseInt(anexo);
		datos[1] = Integer.parseInt(ano);
		return datos;
	}
	//crea los objetos y añada a lista correspondiente
	static void crear(ArrayList<Estudiante> lista_estudiantes,
						ArrayList<Profesor> lista_profesores, 
						ArrayList<Administrativo> lista_administrativos) {
		String dat1;
		Scanner txt = new Scanner(System.in);
		
		System.out.println("¿Que desea añadir? 1: Estudiante, 2: Administrativo y 3: Profesor");
		dat1 = txt.nextLine();
		//año = ano
		String curso, departamento, seccion;
		//se realiza en try en caso de que ingrese un dato invalido
		try {
			int dat = Integer.parseInt(dat1);
			//se llama una función datos para los datos de personas, los cuales están para los 3 casos
			String[] datos = datos_p();
			switch (dat) {
			//caso 1 de si es estudiante
			case 1:
				System.out.println("Ingrese el curso inscrito: ");
				curso = txt.nextLine();
				Estudiante estu = new Estudiante(datos[0], datos[1], datos[2], curso);
				lista_estudiantes.add(estu);
				break;
			//caso 2 si es administrativo, datos e es para los datos de empleados
			case 2:
				int[] dato_e1 = datos_e();  
				System.out.println("Ingrese el departamento afiliado: ");
				seccion = txt.nextLine();
				Administrativo admin = new Administrativo(datos[0], datos[1], datos[2], dato_e1[0], dato_e1[1], seccion);
				lista_administrativos.add(admin);
				break;
			//caso 3 si es profesor, los datos e cumplen la misma funcion que el caso 2
			case 3:
				int[] dato_e2 = datos_e();
				System.out.println("Ingrese la sección en la cual esta afiliado: ");
				departamento = txt.nextLine();
				Profesor profe = new Profesor(datos[0], datos[1], datos[2], dato_e2[0], dato_e2[1], departamento);
				lista_profesores.add(profe);
				break;
			//en caso de que coloque una opcion de numero invalida
			default:
				System.out.println("Opción númerica inválida");
				break;
			}
		} catch (Exception e) {
			//en caso de que presione un string en vez de un numero
			System.out.println("Opción escrita inválida");
		}
		
		
	}
	//main encargado de llamar crear de manera definida por el usuario cuando acabar y buscar
	public static void main(String[] args) {
		ArrayList<Estudiante> lista_estudiantes = new ArrayList<Estudiante>();
		ArrayList<Profesor> lista_profesores = new ArrayList<Profesor>();
		ArrayList<Administrativo> lista_administrativos = new ArrayList<Administrativo>();
		//se crean la lista de profesores, estudiantes o administrativos hasta que se decide acabar
		while (true) {
			crear(lista_estudiantes, lista_profesores, lista_administrativos);
			String dat;
			Scanner txt = new Scanner(System.in);
			System.out.println("Presione q si quiere dejar de añadir:");
			dat = txt.nextLine();
			if(dat.equalsIgnoreCase("q")) {
				break;
			}
		}
		//busca según un criterior preguntado en el mismo metodo
		buscar(lista_estudiantes, lista_profesores, lista_administrativos);
		System.out.println("ha acabado tenga un buen día");
	}
}