package buscador;

public class Profesor extends Persona{
	String departamento;
	//constructor con herencia del profesor
	public Profesor(String Pnombre,String Papellido,String Pidentificacion, int Panexo, int Pincorporado, String departamento) {
		super(Pnombre, Papellido, Pidentificacion, Panexo, Pincorporado, true);
		this.departamento = departamento;
	}
	public String getDepartamento() {
		return departamento;
	}
}