package buscador;

public class Persona {
	String nombre;
	String identificacion;
	String apellido;
	int anexo;
	int incorporado;
	//constructor para estudiantes
	Persona(String nombre,String apellido,String identificacion){
		this.nombre = nombre;
		this.apellido = apellido;
		this.identificacion = identificacion;
	}
	//constructor para empleados por la universidad
	Persona(String nombre,String apellido,String identificacion, int anexo, int incorporado, boolean flag){
		this.nombre = nombre;
		this.apellido = apellido;
		this.identificacion = identificacion;
		this.incorporado = incorporado;
		this.anexo = anexo;
		
	}
	//getters

	public int getAnexo() {
		return anexo;
	}
	public int getIncorporado() {
		return incorporado;
	}
	public String getNombre() {
		return nombre;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public String getApellido() {
		return apellido;
	}
}