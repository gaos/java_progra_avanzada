import java.awt.List;
import java.util.*;




public class simulacion {
	protected String _prueba;
	
	public void randoms_inicial(ArrayList<comunidad> cum,int iniciales, int indice) {
		int contador = 0;
		for (int i = 0; i < cum.get(indice).numero() ; i++) {
			if (cum.get(indice).personas.get(i).enfermos.isEnfermo() == false && contador < iniciales) {
				cum.get(indice).personas.get(i).enfermos.setEnfermo(true);
				contador++;
			}
		}
	}
	
	public int cuenta_enfermos(ArrayList<comunidad> cum, int indice) {
		int contador = 0;
		for (int i = 0; i < cum.get(indice).numero() ; i++) {
			if (cum.get(indice).personas.get(i).enfermos.isEnfermo()) {
				contador++;
			}
		}
		return contador;
		
	}
	
	public void mostrar_ids(ArrayList<comunidad> elegida, int indice) {
		
		for (int i = 0; i < elegida.get(indice).numero() ; i++) {
			System.out.println("ID:" + elegida.get(indice).personas.get(i).getID());
		}
	}
	
	public boolean[] creacion_alterna(ArrayList<comunidad> primera, int indice){
		boolean[] espejo = new boolean[primera.get(indice).personas.size()];
		Arrays.fill(espejo, Boolean.FALSE);
		for (int i = 0; i < primera.get(indice).personas.size(); i++) {
			espejo[i] =primera.get(indice).personas.get(i).enfermos.isEnfermo();
		}
		return espejo;
	}
	
	public boolean[] comparacion(ArrayList<comunidad> lista_personas, boolean[] espejo, boolean[] espejo2, int indice ) {
		Random r2 = new Random();
		/*espejo 2 cambiara, para así cambiar determinados por pasos y que no se cambien los valores propios, personas hasta el fin del paso
		Esto es porque si hay solo un espejo cambiara los datos según ingresen, mientras que al haber doble seguridad
		los datos no se cambiaran en medio proceso de los pasos
		*/
		for (int i = 0; i < espejo.length; i++) {
			int posible = r2.nextInt(espejo.length);
			int prob_r = r2.nextInt(99) + 1;
			int prob_i = r2.nextInt(99) + 1;
			int prob_c = r2.nextInt(99) + 1;


			
			
			if (espejo[posible] && lista_personas.get(indice).personas.get(i).enfermos.isEnfermo()) {
				// no se hace nada porque ambos estan enfermos
				
			} else if ((espejo[posible] && lista_personas.get(indice).personas.get(i).enfermos.isEnfermo() == false) &&
					(lista_personas.get(indice).personas.get(i).enfermos.isPresente() == false) &&
					(prob_i <= lista_personas.get(indice).prob_de_infeccion) && (prob_r <= lista_personas.get(indice).prob_de_reunirse) &&
					(prob_c <= lista_personas.get(indice).prob_de_conexion)) {
				
				// aqui el espejo es posible contagio, la persona podría ser infectada por lo que se compara, se ve si la persona tuvo enferma
					espejo2[i] = true;
				
			} else if ((lista_personas.get(indice).personas.get(i).enfermos.isEnfermo() && espejo[posible] == false) && 
						(lista_personas.get(indice).personas.get(posible).enfermos.isPresente() == false) && (
						(prob_i <= lista_personas.get(indice).prob_de_infeccion) && (prob_r <= lista_personas.get(indice).prob_de_reunirse) &&
						(prob_c <= lista_personas.get(indice).prob_de_conexion))) {
				
				//este es en el caso de que la persona contagie al del espejo, se ve si el posible tuvo la enfermedad
				espejo2[posible] = true;
			}
		}
		return espejo2;
	}
	
	public void cambio(boolean[] espejo2, ArrayList<comunidad> lista_personas, int indice) {
		for (int i = 0; i < espejo2.length; i++) {
			lista_personas.get(indice).personas.get(i).enfermos.setEnfermo(espejo2[i]) ;
		}
	}
	public void añadir_pasos(ArrayList<comunidad> lista_personas, int indice) {
		for (int i = 0; i < lista_personas.get(indice).num_personas; i++) {
			if(lista_personas.get(indice).personas.get(i).enfermos.isEnfermo()) {
				lista_personas.get(indice).personas.get(i).enfermos.setPasos_propios(lista_personas.get(indice).personas.get(i).enfermos.getPasos_propios() + 1);
			}
		}
	}
	
	public void sana_sana_colita_de_rana(ArrayList<comunidad> lista_personas, int indice, int pasos) {
	//si no sana hoy, sana mañana
		for (int i = 0; i < lista_personas.get(indice).num_personas; i++) {
			if(lista_personas.get(indice).personas.get(i).enfermos.getPasos_propios()>= lista_personas.get(indice).pasos_min) {
				lista_personas.get(indice).personas.get(i).enfermos.setEnfermo(false);
			}
		}
	}
	
	
	public simulacion(int rep) {
		ArrayList<comunidad> comunidades = new ArrayList<comunidad>();
			
		for(int inicio = 0; inicio < rep;inicio++) {
		
			String dat1;
			Scanner text = new Scanner(System.in);
			
		
	
			if(inicio == 0) {
				
				try {
					while(true) {
							String dat3, dat4;
							Scanner text2 = new Scanner(System.in);
							System.out.println("Por favor ingrese los datos de la simulación");
							System.out.println("Comunidad:");
							dat1 = text2.nextLine();
							
							System.out.println("Ingrese los pasos minimos para sanarse o morir:");
							dat3 = text2.nextLine();
							int pass = Integer.parseInt(dat3);
	
							System.out.println("Ingrese probabilidad de reunirse % de 1 a 100:");
							dat4 = text2.nextLine();
							int prob = Integer.parseInt(dat4);
							
							System.out.println("Ingrese probabilidad de infeccion % de 1 a 100:");
							dat4 = text2.nextLine();
							int prob_i = Integer.parseInt(dat4);
							
							System.out.println("Ingrese probabilidad de conexion % de 1 a 100:");
							dat4 = text2.nextLine();
							int prob_c = Integer.parseInt(dat4);
							
							comunidad no_judios;
							String person;
							System.out.println("Ingrese la cantidad de personas:");
							
							//razón por la que no he usado nextInt es por un error que hay que ignora las siguientes comunidades en mi pc
							person = text2.nextLine();
							int person_int = Integer.parseInt(person);
							
							no_judios = new comunidad(person_int, dat1, prob, pass, prob_i,prob_c);
	
							comunidades.add(no_judios);
							System.out.println("¿Desea añadir más comunidades? q para salir");
							dat1 = text.nextLine();
							if(dat1.equalsIgnoreCase("q")){
								break;
								}		
							}
				// así se entra a la enfermedad
				//System.out.println(comunidades.get(0).personas.get(0).enfermos.isEnfermo());
					
	
				
				//cuenta los enfermos que hay
				for (int j = 0; j < comunidades.size(); j++) {
					System.out.println("Enfermos iniciales en " + comunidades.get(j).cum_nombre + ":" );
					Scanner text2 = new Scanner(System.in);
					String dat5 = text2.nextLine();
					int iniciales = Integer.parseInt(dat5);
					randoms_inicial(comunidades, iniciales, j);
					
					int cantidad_enfermos = cuenta_enfermos(comunidades, j);
					System.out.println("Los enfermos total que hay en " + comunidades.get(j).cum_nombre + ":" + cantidad_enfermos);
					añadir_pasos(comunidades, j);
					
	
					
				}
				int cantidad_enfermos_t = 0;
				for (int cont = 0; cont < comunidades.size(); cont++) {
					cantidad_enfermos_t = cantidad_enfermos_t + cuenta_enfermos(comunidades, cont);
				}
				
				System.out.println("Los enfermos total que hay en las comunidades: " + cantidad_enfermos_t + " en el paso " + inicio);
				
				for (int indice_cambiar = 0; indice_cambiar < comunidades.size(); indice_cambiar++) {
					boolean[] espejo = creacion_alterna(comunidades, indice_cambiar);
					boolean[] espejo2 = creacion_alterna(comunidades, indice_cambiar);
					espejo2 = comparacion(comunidades, espejo, espejo2, indice_cambiar);
					cambio(espejo2, comunidades, indice_cambiar);
				}
				
				}catch (Exception e) {
					
					System.out.println("Se ha producido un error");
				
				}
		
				
			}else {
				try {
					
					int cantidad_enfermos_t = 0;
					for (int cont = 0; cont < comunidades.size(); cont++) {
						cantidad_enfermos_t = cantidad_enfermos_t + cuenta_enfermos(comunidades, cont);
					}
					System.out.println("Los enfermos total que hay en las comunidades: " + cantidad_enfermos_t + " en el paso " + inicio);
					for (int indice_cambiar = 0; indice_cambiar < comunidades.size(); indice_cambiar++) {
						añadir_pasos(comunidades, indice_cambiar);
						sana_sana_colita_de_rana(comunidades, indice_cambiar, inicio);
						boolean[] espejo = creacion_alterna(comunidades, indice_cambiar);
						boolean[] espejo2 = creacion_alterna(comunidades, indice_cambiar);
						espejo2 = comparacion(comunidades, espejo, espejo2, indice_cambiar);
						cambio(espejo2, comunidades, indice_cambiar);
					}
				} catch (Exception e) {
					System.out.println("Hubo un error");
				}
			}	
		}
	}
	
	public simulacion(){

	}
	
	public String mostrar() {
		return _prueba;
	}
}
