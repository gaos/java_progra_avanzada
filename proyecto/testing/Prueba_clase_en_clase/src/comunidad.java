import java.util.*;

public class comunidad extends simulacion {
	
	int num_personas; 
	ArrayList<gente> personas = new ArrayList<gente>();;
	String cum_nombre;
	int prob_de_infeccion;
	int prob_de_conexion;
	int prob_de_reunirse;
	int pasos_min;

	
	public int getNum_personas() {
		return num_personas;
	}
	public void setNum_personas(int num_personas) {
		this.num_personas = num_personas;
	}
	public ArrayList<gente> getPersonas() {
		return personas;
	}
	public void setPersonas(ArrayList<gente> personas) {
		this.personas = personas;
	}
	public String getCum_nombre() {
		return cum_nombre;
	}
	public void setCum_nombre(String cum_nombre) {
		this.cum_nombre = cum_nombre;
	}
	public int getProb_de_infeccion() {
		return prob_de_infeccion;
	}
	public int getPasos_min() {
		return pasos_min;
	}
	
	
	comunidad(){
		super();
	}
	comunidad(int _personas, String nombre, int prob, int pass, int prob_i, int prob_c) {
		gente personas1;
		this.num_personas = _personas;
		this.cum_nombre = nombre;
		this.prob_de_reunirse = prob;
		this.pasos_min = pass;
		this.prob_de_infeccion = prob_i;
		this.prob_de_conexion = prob_c;
		
		
		for (int i = 0; i < _personas; i++) {
			personas1 = new gente("" + i);
			this.personas.add(personas1);
		}
/*
		for (int i = 0; i < this.personas.size(); i++) {
			System.out.println(this.personas.get(i).getID());
			System.out.println(this.personas.get(i).getEnfermos().isEnfermo());
		}*/
	}
	public int numero() {
		return this.personas.size();
	}

	
	@Override
	public String mostrar() {
		return this.cum_nombre + "||"+this._prueba +"||personas:"+ this.num_personas+"||prob:"+ this.prob_de_infeccion;
	}
}
