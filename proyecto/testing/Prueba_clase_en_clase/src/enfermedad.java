import java.util.*;
public class enfermedad extends gente{
	boolean enfermo;
	
	//presente indica si estuvo la enfermedad
	boolean presente;
	int pasos_propios = 0;
	
	public enfermedad(){
		super();
		//Random r = new Random();
		this.enfermo = false;
		//this.pasos_propios = r.nextInt(100);
		
	}
	public enfermedad(boolean actividad){
		//actividad es si esta activa la enfermedad
		this.enfermo = actividad;
	}
	
	public boolean isEnfermo() {
		return enfermo;
	}
	public void setEnfermo(boolean enfermarse) {
		this.enfermo = enfermarse;
		if(enfermarse) {
			this.presente = true;
		}
	}
	public boolean isPresente() {
		return presente;
	}
	public void setPresente(boolean presente) {
		this.presente = presente;
	}
	
	
	public int getPasos_propios() {
		return pasos_propios;
	}
	public void setPasos_propios(int pasos_propios) {
		this.pasos_propios = pasos_propios;
	}
	@Override
	public String mostrar() {
		return cum_nombre + _prueba +"\n"+ personas +"\n" +"\n "+ enfermo;
	}
}
