package plague;
import java.util.*;
import plague.persona;

public class inicio {
/*Hacer un plage.inc
 * cada persona da pasos
 * */
	static int infectados(ArrayList<persona> personas,int contador) {
		for (int i = 0; i < personas.size(); i++) {
			if(personas.get(i).isEnfermo() == true) {
				contador++;
			}
			
		}
		return contador;
	}
	static void paseo(ArrayList<persona> pobla){
	Random r2 = new Random();
	int posible = r2.nextInt(99) + 1;
	/* usar un array para obtener randoms segun la cantidad de personas en poblacion
	 de esta forma se ocupará de manera random cada paso, con la ubicacion de las personas
	 al estar dos personas en una ubicación influiran si uno es enfermo, si uno es enfermo entonces
	se obtiene un numero, si es inferior al contacto ambos, entonces se tocan, al tocarse se obtiene
	el porcentaje de contagiarse, en el caso de que ambos esten enfermos, no se considera el proceso
*/
	
// i persona, j pasos	
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if(pobla.get(i).getPasos() <j){
			// aqui se crea un random por paso
				int paso_random = r2.nextInt(20)+1;
				pobla.get(i).setRandom_por_paso(paso_random);
			}
		}
	}
	
	}
	
	public static void main(String[] args) {
		/*
		Random r = new Random();
		System.out.println(r);
		int valorDado = r.nextInt(2);
		System.out.println(valorDado);
		Como hacer un random
		*/
		ArrayList<persona> poblacion = new ArrayList<persona>();
		
		Random r = new Random();
		
		
		int contador = 0;
		for (int i = 0; i < 10; i++) {
			
			if (i == 0) {
					
				int valor_pasos = r.nextInt(9) + 1;
				persona sujeto;
				sujeto = new persona();
				sujeto.setEnfermo(true);
				
				int valor_contacto = r.nextInt(100);
				
				
				sujeto.setContacto(valor_contacto);
				sujeto.setPasos(valor_pasos);

				poblacion.add(sujeto);
			
			}else {
				int valor_pasos = r.nextInt(9) + 1;
				persona sujeto;
				sujeto = new persona();

				int valor_contacto = r.nextInt(100);
				
				//el contacto es el porcentaje de contacto que tiene cada persona
				sujeto.setContacto(valor_contacto);
				sujeto.setPasos(valor_pasos);
				
				poblacion.add(sujeto);
			}
		}
		System.out.println("La cantidad de personas en la población es:" + poblacion.size());
		
		for (int j=0; j<poblacion.size();j++) {
			
			System.out.println("Persona:" + (j+1));
			System.out.println("pasos:" + poblacion.get(j).getPasos());
			System.out.println("estado:" + poblacion.get(j).isEnfermo());
			System.out.println("Contactos:"+ poblacion.get(j).getContacto());
			
			}
		contador = infectados(poblacion, contador);
		System.out.println("Los infectados son:" + contador);

		
	}

}
