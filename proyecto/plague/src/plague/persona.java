package plague;

public class persona {
	private int pasos;
	private int contacto;
	private boolean enfermo;
	private int random_por_paso;
	
	public int getRandom_por_paso() {
		return random_por_paso;
	}

	public void setRandom_por_paso(int random_por_paso) {
		this.random_por_paso = random_por_paso;
	}

	persona(){
		this.pasos = 0;
		this.enfermo = false;
		this.contacto = 0;
		this.random_por_paso = 0;
	}

	public int getPasos() {
		return pasos;
	}

	public void setPasos(int pasos) {
		this.pasos = pasos;
	}

	public int getContacto() {
		return contacto;
	}

	public void setContacto(int contacto) {
		this.contacto = contacto;
	}

	public boolean isEnfermo() {
		return enfermo;
	}

	public void setEnfermo(boolean enfermo) {
		this.enfermo = enfermo;
	}
	
}
