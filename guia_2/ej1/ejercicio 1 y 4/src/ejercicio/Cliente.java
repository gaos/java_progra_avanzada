package ejercicio;

class Cliente {
	private String nombre;
	private String contraseña;
	private int saldo;
	
	Cliente(String para_name, int para_value){
		this.nombre = para_name;
		this.saldo = para_value;
		this.contraseña = "1234";
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public void setnombre(String nombre) {
		this.nombre = nombre;
	}

	public int getSaldo() {
		return this.saldo;
	}

	public String getNombre() {
		return this.nombre;
	}
	
	public void setcontra(String contra) {
		this.contraseña = contra;
	}

	public String getcontra() {
		return this.contraseña;
	}
	
}