package ejercicio;
import java.util.*; //para Scanner, así tener un input
import ejercicio.Cliente;

/*
 * debido a la similitud del 1 y 4, los he dejado juntos
 * y en vez de numero de cuenta, decidí utilizar una contraseña, debido a que es más habitual para
 * las transferencias el uso de contraseña en vez de un número
 * */

public class main {
	
	//verifica si la contraseña y usuario son correctos
	static boolean verificador(ArrayList<Cliente> cuentas, String titular, String contraseña) {
		boolean flag = false;
		for (int i=0; i< cuentas.size();i++) {
			if (titular.equals(cuentas.get(i).getNombre()) && contraseña.equals(cuentas.get(i).getcontra())) {
				
				flag = true;
				break;
			}
		}
		return flag;		
	}
	
	//busca la posición de una cuenta y si no está entrega un valor negativo para que no entre
	static int buscador(ArrayList<Cliente> cuentas, String titular) {
		int posicion = -1;
		for (int i=0; i< cuentas.size();i++) {
			if (titular.equals(cuentas.get(i).getNombre())) {
				posicion = i;
				break;
			}
		}
		return posicion;
	}
	
	//transferencia de una cuenta a otra
	static void trasnferencia(ArrayList<Cliente> cuentas) {
		
		Scanner text = new Scanner(System.in);
		String dat1, dat2, dat4;
		int dat3 = 0;
		
		//se verifica la cuenta
		System.out.println("Por favor ingrese los datos de su cuenta");
		System.out.println("Cuenta:");
		dat1 = text.nextLine();
		System.out.println("Contraseña:");
		dat2 = text.nextLine();
		
		//en caso de que sea correcto
		if (verificador(cuentas,dat1,dat2) == true) {
			
			//se pide un monto
			System.out.println("Por favor ingrese monto a transferir");
			dat3 = text.nextInt();
			
			//se observa si el monto está disponible
			if (dat3 <= cuentas.get(buscador(cuentas,dat1)).getSaldo() ) {
				
				System.out.println("será transferido a la cuenta:");
				dat4 = text.nextLine();
				
				//en caso lo que esté se busca la cuenta y si esta se realiza la transferencia
				if (0 <= buscador(cuentas, dat4)) {
					cuentas.get(buscador(cuentas, dat4)).setSaldo(cuentas.get(buscador(cuentas, dat4)).getSaldo() + dat3);
				}else {
					System.out.println("No se encontró la cuenta mencionada");
				}
			}else {
				System.out.println("No cuenta con el saldo suficiente para la transferencia");
			}
		}else {
			System.out.println("Ingreso datos no válidos");
		}
	}

	
	//cambia el valor de una cuenta
	static void cambio_saldo(ArrayList<Cliente> cuentas, boolean flag) {
		
		Scanner text = new Scanner(System.in);
		String dat1,dat2;
		int dat3 =0;
		
		System.out.println("Por favor ingrese los datos de su cuenta");
		System.out.println("Cuenta:");
		dat1 = text.nextLine();
		System.out.println("Contraseña:");
		dat2 = text.nextLine();
		
		if (verificador(cuentas,dat1,dat2) == true) {
			
			System.out.println("¿Cuanto desea ");
			
			if(flag == true) {
			
			System.out.print("abonar?:");
			
			}else {
				
				System.out.print("descontar?:");
			}
			dat3 = text.nextInt();
			
			if (dat3 >= 0 && flag == true) {
				
				cuentas.get(buscador(cuentas, dat1)).setSaldo(cuentas.get(buscador(cuentas, dat1)).getSaldo() + dat3);
			
			}else if (dat3 >= 0 && flag == false){
				
				int resultado = cuentas.get(buscador(cuentas, dat1)).getSaldo() - dat3;
				
				if (resultado >= 0) {
					
					cuentas.get(buscador(cuentas, dat1)).setSaldo(cuentas.get(buscador(cuentas, dat1)).getSaldo() - dat3);
						
				}else {
					System.out.println("Ingreso una cantidad que excede al saldo, volverá al inicio");
				}
			}else {
				System.out.println("Ingreso una cantidad inadecuada, volverá al inicio");
			}
		}
	}
	
	//se encarga de crear un usuario con contraseña y nombre
	static void creacion(ArrayList<Cliente> cuentas) {

		
		for (int i = 0; i <10; i++) {
			
			try {
				Scanner text = new Scanner(System.in);
				String opc1 = "";
				int opc2 = 0;
				String opc3 = "";
				
				System.out.println("Nombre del titular:");
				opc1 = text.nextLine();
		
				System.out.println("Contraseña:");
				opc3 = text.nextLine();
				
				System.out.println("Saldo inicial:");
				opc2 = text.nextInt();
				
				if (opc2 >=0) {
				
					Cliente tarjeta1;
					
					tarjeta1 = new Cliente(opc1, opc2);
					tarjeta1.setcontra(opc3);
					
					cuentas.add(tarjeta1);
					
			//		tarjeta1.setnombre(opc1);
					String t = tarjeta1.getNombre();
					int p = tarjeta1.getSaldo();
					
					System.out.println("El titular es:");
					System.out.println(t);
					System.out.println("Saldo es:");
					System.out.println(p);
					
					
					System.out.println(cuentas.get(cuentas.size()-1));
					
					for (int j=0; j<cuentas.size();j++) {
						cuentas.get(j);
					}
					i = 10;
				}
				//equals es para comparar caracteres/strings
			
			
			}catch (Exception e){
		
				System.out.println("Hubo un error, espere en línea");
				
				i = 1;
			}
		}
	}
	
	static void menu() {
		
		ArrayList<Cliente> lista_cuentas = new ArrayList<Cliente>();
		
		while(true) {
			//se genera array list
			
			//el input
			Scanner text = new Scanner(System.in);
			String opcion;
			
			System.out.println("\tBienvenido a banca en línea");
			System.out.println("1.¿Desea crear nueva cuenta?");
			System.out.println("2. Salir");
			System.out.println("3. Registro de cuentas");
			System.out.println("4. Aumentar Saldo");
			System.out.println("5. Descontar Saldo");
			System.out.println("6. Transferencia");
			opcion = text.nextLine();
			
			if (opcion.equals("1")) {
				
				creacion(lista_cuentas);
				
			}else if(opcion.equals("2")) {
				
				System.out.println("Hasta la próxima");
				break;
				
			}else if(opcion.equals("3")){
				
				if(lista_cuentas.size() != 0) {
					
					System.out.println("La cantidad de cuentas registradas es:" + lista_cuentas.size());
					
					for (int j=0; j<lista_cuentas.size();j++) {
						
						System.out.println("Nombre:" + lista_cuentas.get(j).getNombre());
						System.out.println("Saldo:" + lista_cuentas.get(j).getSaldo());
						System.out.println("Contraseña:" + lista_cuentas.get(j).getcontra());
						}
				
				}else {
					
						System.out.println("No hay cuentas registradas");
					
				}
				}else if(opcion.equals("4")){
				
					cambio_saldo(lista_cuentas, true);
				
				}else if(opcion.equals("5")){
					
					cambio_saldo(lista_cuentas, false);
				
				} else if (opcion.equals("6")){
					
				}else {
				System.out.println("Opción inválida");
			}
		}
	}

	public static void main(String[] args) {
		
		menu();
				

	}

}