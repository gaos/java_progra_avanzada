package autito;
import java.util.*;

public class main {
	//supuestamente limpia la pantalla, no lo puedo comprobar desde que mi terminar no ve el programa
	//compruebo el programa con la consola de eclipse
    public static void clearScreen() {  

        System.out.print("\033[H\033[2J");  

        System.out.flush();  

     } 
    //enciende el auto y cada vez que lo hace descuenta el 1% de lo que tenga el estanque
    public static void encender_auto(rasho_macuin auto) {
		double estanque_porciento;
		auto.setEstado(true);
		estanque_porciento = auto.estanque.getVolumen()*0.01;
		auto.estanque.setVolumen((auto.estanque.getVolumen() - estanque_porciento));
	}
	//ve las ruedas y las altera por el desgaste
	public static void estado_rueda(rasho_macuin auto) {
		Random r = new Random();
		int rueda1 = r.nextInt(9)+1;
		int rueda2 = r.nextInt(9)+1;
		int rueda3 = r.nextInt(9)+1;
		int rueda4 = r.nextInt(9)+1;
		
		auto.rueda_ne.setEstado(auto.rueda_ne.getEstado() - rueda1);
		auto.rueda_no.setEstado(auto.rueda_no.getEstado() - rueda2);
		auto.rueda_se.setEstado(auto.rueda_se.getEstado() - rueda3);
		auto.rueda_so.setEstado( auto.rueda_so.getEstado() - rueda4);
	}
	//lo unico que hace es verificar para detener el auto;
	public static boolean verificador(rasho_macuin auto) {
		if (auto.rueda_ne.getEstado() <=10 || auto.rueda_no.getEstado() <=10 
				|| auto.rueda_se.getEstado() <=10 || auto.rueda_so.getEstado() <= 10) {
			return true;
		}else {
			return false;
		}
	}
	//cambia la rueda por una nueva generando un nuevo objeto rueda como pide el enunciado
	public static void cambio_de_rueda(rasho_macuin auto) {
		if (auto.rueda_ne.getEstado() <=10) {
			rad rueda_repuesto1 = new rad(100);
			auto.setRueda_ne(rueda_repuesto1);
		}
		if (auto.rueda_no.getEstado() <=10) {
			rad rueda_repuesto2 = new rad(100);
			auto.setRueda_no(rueda_repuesto2);
		}
		if (auto.rueda_se.getEstado() <=10) {
			rad rueda_repuesto3 = new rad(100);
			auto.setRueda_se(rueda_repuesto3);
		}
		if (auto.rueda_so.getEstado() <=10) {
			rad rueda_repuesto4 = new rad(100);
			auto.setRueda_so(rueda_repuesto4);
		}
	}
	//dibuja según el contador del avance del programa
	public static void movimiento(int contador) {
		for(int i=1; i<contador;i++) {
			System.out.print("=");
		}
		System.out.print("D");
		System.out.println("\nEl auto D\n");
	}
	//función para disminuir el estanque según el avance
	public static void gasto(rasho_macuin auto, int distancia) {
		if(auto.motorzote.getCilindrada() == 1.2) {
			auto.estanque.setVolumen(auto.estanque.getVolumen() - (distancia/20));
		}else {
			auto.estanque.setVolumen(auto.estanque.getVolumen() - (distancia/14));
		}
	}
	public static void main(String[] args) {
		
		Scanner text = new Scanner(System.in);
		String dat1;
		rasho_macuin auto = new rasho_macuin();
		auto.mostrar();
		int count = 0;
		while(true) {
			System.out.println("¿Desea encender el motor? s para si");
			dat1 = text.nextLine();
			if(dat1.equalsIgnoreCase("s")) {
				
				encender_auto(auto);
				if (verificador(auto)) {
					cambio_de_rueda(auto);
				}
				
				//auto andando
				while(auto.isEstado() && auto.estanque.getVolumen() > 0) {
					clearScreen();
					System.out.println("\nEl contador:"+count);
					count = count +1;
					movimiento(count);
					Random r = new Random();
					int tiempo = r.nextInt(9)+1;
					int distancia = r.nextInt(119)+1;
					auto.setDistancia_recorrida(distancia);
					auto.veloci.setVelocidad(distancia / tiempo);
					auto.setHoras_que_estuvo(tiempo);
					estado_rueda(auto);
					gasto(auto, distancia);
					auto.mostrar();
					if(verificador(auto)) {
						auto.setEstado(false);
						break;
					}
					String dat2;
					System.out.println("presione una tecla para continuar");
					dat2 = text.nextLine();
				}
				if(auto.estanque.getVolumen() <= 0) {
					auto.setEstado(false);
					System.out.println("Se quedo sin combustible, adiós");
					break;
				}
			}else {
				System.out.println("Entendible, tenga un buen día");
				break;
			}
		}
	}
}
