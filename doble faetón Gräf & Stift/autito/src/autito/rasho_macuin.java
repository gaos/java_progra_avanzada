package autito;
import java.util.*;

public class rasho_macuin {
	velocimetro veloci;
	rad rueda_no;
	rad rueda_ne;
	rad rueda_so;
	rad rueda_se;
	Estanque estanque;
	int distancia_recorrida;
	motor motorzote;
	boolean estado;
	int horas_que_estuvo;
	
	public rasho_macuin() {
		
		Random r = new Random();
		int posible = r.nextInt(2);

		if (posible == 1) {
			this.motorzote = new motor(1.2);
		} else {
			this.motorzote = new motor(1.6);
		}
		this.estanque = new Estanque();
		this.distancia_recorrida = 0;
		this.rueda_no = new rad(100);
		this.rueda_ne = new rad(100);
		this.rueda_so = new rad(100);
		this.rueda_se = new rad(100);
		this.estado = false;
		this.veloci = new velocimetro(0);
		this.horas_que_estuvo = 0;
	}
	
	public void mostrar() {
		System.out.println("El estado del auto es:");
		System.out.println("Con un increíble motor de: " + this.motorzote.getCilindrada());
		System.out.println("Su kilometraje es de: " + this.distancia_recorrida);
		System.out.println("Su estanque: "+this.estanque.getVolumen());
		System.out.println("Su rueda delantera derecha esta al: "+this.rueda_ne.getEstado()+"%");
		System.out.println("Su rueda delantera izquierda esta al: "+this.rueda_no.getEstado()+"%");
		System.out.println("Su rueda trasera derecha esta al: "+this.rueda_so.getEstado()+"%");
		System.out.println("Su rueda trasera izquierda esta al: "+this.rueda_se.getEstado()+"%");
		System.out.println("El velocimetro marca: "+this.veloci.getVelocidad()+"km/h");
		System.out.println("Estuvo "+horas_que_estuvo+" horas al volante");
		if(estado) {
			System.out.println("Con el motor está encendido");
		}else {
			System.out.println("Con el motor apagado");
		}
	}

	


	public boolean isEstado() {
		return estado;
	}

	public void setRueda_no(rad rueda_no) {
		this.rueda_no = rueda_no;
	}

	public void setRueda_ne(rad rueda_ne) {
		this.rueda_ne = rueda_ne;
	}

	public void setRueda_so(rad rueda_so) {
		this.rueda_so = rueda_so;
	}

	public void setRueda_se(rad rueda_se) {
		this.rueda_se = rueda_se;
	}

	
	public void setHoras_que_estuvo(int horas_que_estuvo) {
		this.horas_que_estuvo = horas_que_estuvo;
	}


	public void setDistancia_recorrida(int distancia_recorrida) {
		this.distancia_recorrida = this.distancia_recorrida + distancia_recorrida;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
}
